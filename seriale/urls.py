from django.conf.urls import patterns, include, url
from django.views.generic.simple import direct_to_template

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
import settings

urlpatterns = patterns('',
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.STATIC_ROOT}),
    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^opis_z_wiki/', include('opis_z_wiki.urls')),
    url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^users/', include('users.urls')),
    url(r'^$', 'seriale.views.index_page', name='index'),
    url(r'^about', 'seriale.views.about_page', name='about' ),
    url(r'^comments/', include('django.contrib.comments.urls')),
    url(r'^access-denied/$', direct_to_template,  { 'template': 'access_denied.html' }, 'access-denied'),
    url(r'^tv_show/', include('tv_show.urls')),
    url(r'^season/', include('season.urls')),
    url(r'^episode/', include('episode.urls')),
    url(r'^analytics/', include('analytics.urls')),
    url(r'img/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT,
        }),

)

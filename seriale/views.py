from django.template.response import TemplateResponse
from tv_show.models import TvShow
from analytics.middleware import use_in_analytics

@use_in_analytics
def index_page(request):
    context = {'tv_shows': TvShow.objects.all()}
    return TemplateResponse(request, 'index.html', context )

@use_in_analytics
def about_page(request):
    return TemplateResponse(request, 'about.html', {})
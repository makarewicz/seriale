/*
function getExpirationString(days) {
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    return "; expires="+date.toGMTString();
}

function getCookieName(editor) {
    return username + ed.id.toString();
}

function
function saveEditorContentInCookie(editor) {
    if (username === undefined)
        username = "unknown"

    document.cookie = name+"="+value+expires+"; path=/";

    //tinyMCE.get('id_comment').getContent()
}
function loadEditorFromCookie(editor) {
    if (username === undefined)
        username = "unknown"


}
*/
tinyMCE.init({
    // General options
    mode : "textareas",
    theme : "advanced",
    plugins : "tinyautosave,autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
    // Theme options
    theme_advanced_buttons1 : "tinyautosave, save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
    theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
    theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    tinyautosave_minlength : 1,



// Saving content into cookies
    /*
    setup : function(editor) {
        ed.onInit.add(function(ed) {

        });
        ed.onChange.add(function(ed, l) {
            //alert(ed.getContent());
            if (typeof(username) === 'undefined')
                username = "name"
            createCookie(username + ed.id.toString() , ed.getContent(), 2);
        });
    }
    */
});

var total_time_on_site = 0.0;

function send(url) {
   $.ajax({
        url: url,
        data: {'time': total_time_on_site, 'url' : location.href, 'selected_layout': localStorage.getItem('selectedLayout')},
        async : false
    });
}



window.onload = function () {send("/analytics/site_rendered/"); setInterval(function(){total_time_on_site = total_time_on_site + 0.1},100); }
window.onbeforeunload = function () {send("/analytics/site_left/"); }
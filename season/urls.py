from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('season.views',
    url(r'^(?P<pk>\d+)', 'season_page'),
)
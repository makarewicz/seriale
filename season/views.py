from django.template.response import TemplateResponse
from season.models import Season
from analytics.middleware import use_in_analytics

@use_in_analytics
def season_page(request, pk):
    s = Season.objects.get(pk=pk)
    context = {'season':s,
               'episodes':s.episode_set.order_by('number')}
    return TemplateResponse(request, 'season.html', context )
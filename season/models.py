from django.db import models
from tv_show.models import TvShow
from actor.models import Actor

class Season(models.Model):
    tv_show = models.ForeignKey(TvShow, verbose_name='Serial')
    number = models.IntegerField()
    premiere_date = models.DateField(verbose_name='Data premiery', blank=True, null=True)
    expected_episodes_count = models.IntegerField(verbose_name='Spodziewanwa liczba odcinkow', blank=True, null=True)
    actors = models.ManyToManyField(Actor, verbose_name='Aktorzy wystepujacy w tym sezonie', blank=True, null=True)

    def __unicode__(self):
        return str(self.tv_show) + ' S' + str(self.number)

    def tv_show_name(self):
        return self.tv_show.name

    tv_show_name.admin_order_field = 'tv_show__name'
    tv_show_name.short_description = 'Nazwa serialu'

    def real_number_of_episodes(self):
        return self.episode_set.count()

    real_number_of_episodes.short_description = 'Liczba odcinkow'

    def get_absolute_url(self):
        return '/season/%d/' % self.pk
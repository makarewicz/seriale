"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from season.models import Season
from tv_show.models import TvShow
from episode.models import Episode
from actor.models import Actor
from django.utils import timezone

class SeasonTest(TestCase):
    def setUp(self):
        self.ts = TvShow(name="Dobry Show")
        self.ts.save()
        self.s = Season(tv_show=self.ts, premiere_date=timezone.now(), number=2)
        self.s.save()
        self.s2 = Season(tv_show=self.ts, premiere_date=timezone.now(), number=3)
        self.s2.save()
        self.e = Episode(season=self.s, number=1)
        self.e.save()
        self.e2 = Episode(season=self.s, number=2)
        self.e2.save()
        self.e3 = Episode(season=self.s2, number=3)
        self.e3.save()
        self.actors = [Actor(name="Brad Pitt"), Actor(name="Tuna Turner"), Actor(name="Zombie Jack")]
        for a in self.actors:
            a.save()
            self.s.actors.add(a)
            self.s.save()

    def test_total_number_of_episodes(self):
        self.assertEqual(self.s.real_number_of_episodes(), 2)
        self.assertEqual(self.s2.real_number_of_episodes(), 1)

    def test_unicode(self):
        self.assertEqual(str(self.s), "Dobry Show S2")
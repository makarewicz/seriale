from season.models import Season
from episode.models import Episode
from django.contrib import admin

class EpisodeInline(admin.TabularInline):
    model = Episode
    extra = 0

class SeasonAdmin(admin.ModelAdmin):
    list_display = ('tv_show_name', 'number', 'real_number_of_episodes')
    search_fields = ['tv_show__name', 'number']
    date_hierarchy = 'premiere_date'
    inlines = [EpisodeInline]


admin.site.register(Season, SeasonAdmin)
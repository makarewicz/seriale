from episode.models import Episode
from django.contrib import admin
from django.http import HttpResponse
from django.core import serializers
from tv_show.admin import copy_from_wiki, resize_picture


def copy_actors_from_season(modeladmin, request, queryset):
    for episode in queryset:
        episode.copy_actors_from_season()
copy_actors_from_season.short_description = "Kopiuj liste aktorow z sezonu"


class EpisodeAdmin(admin.ModelAdmin):
    list_display = ('tv_show_name', 'season_number', 'episode_number')
    search_fields = ['season__tv_show__name', 'season__number', 'number']
    date_hierarchy = 'premiere_date'
    actions = [copy_actors_from_season, copy_from_wiki, resize_picture]
    class Media:
        js = ('/static/tiny_mce/tiny_mce.js',
              '/static/tiny_mce/tiny_mce_init.js',
            )

admin.site.register(Episode, EpisodeAdmin)
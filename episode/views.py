from django.template.response import TemplateResponse
from episode.models import Episode
from analytics.middleware import use_in_analytics

@use_in_analytics
def episode_page(request, pk):
    e = Episode.objects.get(pk=pk)
    context = {'episode':e}
    return TemplateResponse(request, 'episode.html', context )
from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('episode.views',
    url(r'^(?P<pk>\d+)', 'episode_page'),
)
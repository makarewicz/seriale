"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from season.models import Season
from tv_show.models import TvShow
from episode.models import Episode
from actor.models import Actor
from django.utils import timezone

class CopyingFromActorsFromSeasonTest(TestCase):
    def setUp(self):
        self.ts = TvShow()
        self.ts.save()
        self.s = Season(tv_show=self.ts, premiere_date=timezone.now(), number=2)
        self.s.save()
        self.e = Episode(season=self.s, number=1)
        self.e.save()
        self.actors = [Actor(name="Brad Pitt"), Actor(name="Tuna Turner"), Actor(name="Zombie Jack")]
        for a in self.actors:
            a.save()
            self.s.actors.add(a)
            self.s.save()

    def test_copy_actors_from_season(self):
        """
        Tests copying actors from season to episode.
        """
        self.e.copy_actors_from_season()
        self.assertItemsEqual(self.e.actors.all(), self.actors)


from django.db import models
from season.models import Season
from actor.models import Actor
from django.utils import timezone



class Episode(models.Model):
    season = models.ForeignKey(Season)
    number = models.IntegerField(verbose_name='Numer odcinka')
    premiere_date = models.DateField(verbose_name='Data premiery', blank=True, null=True)
    image = models.ImageField(upload_to='episode', verbose_name='Obrazek powiazany z odcinkiem', blank=True, null=True)
    actors = models.ManyToManyField(Actor, verbose_name='Aktorzy wystepujacy w tym odcinku', blank=True, null=True)
    description = models.TextField(verbose_name='Opis odcinka', blank=True, null=True)

    def copy_actors_from_season(self):
        self.actors = self.season.actors.all()
        self.save()

    def __unicode__(self):
        return str(self.season) + 'E' + str(self.number)

    def tv_show_name(self):
        return self.season.tv_show.name

    tv_show_name.admin_order_field = 'season__tv_show__name'
    tv_show_name.short_description = 'Nazwa serialu'

    def season_number(self):
        return self.season.number

    season_number.admin_order_field = 'season__number'
    season_number.short_description = 'Numer sezonu'

    def episode_number(self):
        return 'S' + str(self.season_number()) + 'E' + str(self.number)

    episode_number.short_description = 'Numer Odcinka'
    episode_number.admin_order_field = 'number'

    def get_absolute_url(self):
        return '/episode/%d/' % self.pk
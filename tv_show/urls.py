from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('tv_show.views',
    url(r'^(?P<pk>\d+)', 'tv_show_page'),
)
from django.template.response import TemplateResponse
from tv_show.models import TvShow
from analytics.middleware import use_in_analytics

@use_in_analytics
def tv_show_page(request, pk):
    ts = TvShow.objects.get(pk=pk)
    context = {'tv_show':ts,
               'seasons':ts.season_set.order_by('number')}
    return TemplateResponse(request, 'tv_show.html', context )
from tv_show.models import TvShow
from django.contrib import admin
from django.contrib.admin import helpers
from django.template.response import TemplateResponse
from opis_z_wiki.views import get_description_from_wiki_raw
import Image

def copy_from_wiki(modeladmin, request, queryset):
    if request.POST.get('post'):
        for m in queryset:
            if request.POST.get('wiki_art_' + str(m.pk)):
                m.description = str(get_description_from_wiki_raw(request.POST.get('wiki_art_' + str(m.pk)), False))
                m.save()
    # process the queryset here
    else:
        context = {
            'title': ("Kopiowanie opisow"),
            'queryset': queryset,
            'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
            "action_name": "copy_from_wiki",
            }
        return TemplateResponse(request, 'admin/tv_show/copy_from_wiki.html',
            context, current_app=modeladmin.admin_site.name)

def resize_picture(modeladmin, request, queryset):
    for obj in queryset:
        path = obj.image.path
        image = Image.open(path)
        new_image = image.resize((320, 320), Image.ANTIALIAS)
        new_image.save(path)

class TvShowAdmin(admin.ModelAdmin):
    list_display = ('name', 'real_number_of_seasons')
    search_fields = ['name']
    actions = [copy_from_wiki, resize_picture]
    class Media:
        js = ('/static/tiny_mce/tiny_mce.js',
              '/static/tiny_mce/tiny_mce_init.js',
              )

admin.site.register(TvShow, TvShowAdmin)
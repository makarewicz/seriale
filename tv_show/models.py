from django.db import models

class TvShow(models.Model):
    name = models.CharField(max_length=100, verbose_name='Nazwa serialu')
    description = models.TextField(verbose_name='Opis serialu', blank=True, null=True)
    image = models.ImageField(upload_to='tv_show/', verbose_name='Obrazek powiazany z serialem', blank=True, null=True)
    def __unicode__(self):
        return self.name

    def real_number_of_seasons(self):
        return self.season_set.count()

    real_number_of_seasons.short_description = 'Liczba sezonow'

    def get_absolute_url(self):
        return '/tv_show/%d/' % self.pk

    def __unicode__(self):
        return str(self.name)
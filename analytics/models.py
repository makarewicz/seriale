from django.db import models
from django.contrib import admin

class ViewRequested(models.Model):
    address = models.CharField(max_length=100)
    view_name = models.CharField(max_length=100)
    date = models.DateTimeField()
    def __unicode__(self):
        return unicode(self.address) + ' ' + unicode(self.date)

class SiteRendered(models.Model):
    address = models.CharField(max_length=100)
    date = models.DateTimeField()
    def __unicode__(self):
        return unicode(self.address) + ' ' + unicode(self.date)

class SiteLeft(models.Model):
    address = models.CharField(max_length=100)
    date = models.DateTimeField()
    total_time_on_site = models.FloatField()
    selected_layout = models.CharField(max_length=20)
    def __unicode__(self):
        return unicode(self.address) + ' ' + unicode(self.date)

admin.site.register(ViewRequested)
admin.site.register(SiteRendered)
admin.site.register(SiteLeft)
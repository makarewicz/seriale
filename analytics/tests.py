"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from analytics.models import ViewRequested, SiteLeft, SiteRendered
from django.test.client import Client


class AnalyticsTest(TestCase):
    def make_full_requests(self, uri, repeat):
        for i in xrange(repeat):
            self.c.get(uri)
            absolute_uri = 'http://testserver' + uri
            self.c.get('/analytics/site_rendered/', {'url': absolute_uri})
            self.c.get('/analytics/site_left/', {'url': absolute_uri, 'time': 3.0, 'selected_layout': 't1'})

    def setUp(self):
        self.c = Client()
        # Get index.
        self.make_full_requests('/about/', 20)
        self.make_full_requests('/', 10)
        # This shan't count into statistics.
        self.make_full_requests('/analytics/show_stats/', 100)


    def test_analytics_basic(self):
        self.assertEqual(ViewRequested.objects.count(), 30)

    def test_analytics_query(self):
        self.assertEqual(ViewRequested.objects.all().filter(address='http://testserver/about/').count(), 20)
        self.assertEqual(SiteRendered.objects.all().filter(address='http://testserver/about/').count(), 20)
        self.assertEqual(SiteLeft.objects.all().filter(address='http://testserver/about/').count(), 20)


from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('analytics.views',
    url(r'^site_rendered', 'site_rendered'),
    url(r'^site_left', 'site_left'),
    url(r'^show_stats', 'show_stats', name='show_stats')
)
from analytics.models import SiteRendered, SiteLeft, ViewRequested
from django.http import HttpResponse
from django.template.response import TemplateResponse
from django.db.models.aggregates import Avg, Max, Count
from django.utils import timezone
from django.contrib.admin.views.decorators import staff_member_required


def site_rendered(request):
    entry = SiteRendered(address=request.REQUEST['url'], date=timezone.now())
    entry.save()
    return HttpResponse('')

def site_left(request):
    entry = SiteLeft(
        address=request.REQUEST['url'],
        total_time_on_site=request.REQUEST['time'],
        date=timezone.now(),
        selected_layout=request.REQUEST['selected_layout']
    )
    entry.save()
    return HttpResponse('')

@staff_member_required
def show_stats(request):
    most_popular_sites = SiteRendered.objects.values('address').annotate(cnt=Count('date')).order_by('-cnt')[:10]
    layouts = SiteLeft.objects.values('selected_layout').annotate(cnt=Count('date')).annotate(avg_time=Avg('total_time_on_site')).order_by('-avg_time')[:3]
    site_renders = SiteRendered.objects.count()
    view_requests = ViewRequested.objects.count()
    avg_time_on_site = SiteLeft.objects.all().aggregate(Avg('total_time_on_site'))['total_time_on_site__avg']
    context = {'most_popular_sites': most_popular_sites,
               'layouts': layouts,
               'site_renders': site_renders,
               'view_requests': view_requests,
               'avg_time_on_site': avg_time_on_site,
               }
    return TemplateResponse(request, 'show_stats.html', context )
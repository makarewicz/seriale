__author__ = 'michal'
from seriale import settings
from analytics.models import ViewRequested
from django.utils.decorators import decorator_from_middleware
from django.utils import timezone

class AnalysisMiddleware(object):
    def __init__(self):
        'Constructor for GoogleAnalyticsMiddleware'

        try:
            self.html = """<script type="text/javascript" src="/static/js/analytics.js"></script>"""
            self.process_response = self.insert_analytics
            try:
                ignore = settings.ANALYTICS_IGNORE_ADMIN
                if ignore is True:
                    self.process_response = self.ignore_admin(self.process_response)
            except AttributeError:
                pass
        except AttributeError:
            self.process_response = self.return_unaltered

    def insert_analytics(self, request, response):
        content = response.content
        index = content.upper().find('</HEAD>')
        if index == -1: return response
        response.content =  content[:index] + self.html + content[(index):]
        return response

    def return_unaltered(self, request, response):
        return response

    @staticmethod
    def ignore_admin(func):
        def ignore_if_admin(request, response):
            if request.user.is_authenticated() and request.user.is_staff:
                return response
            else:
                return func(request, response)
        return ignore_if_admin

    def process_view(self, request, view_func, view_args, view_kwargs):
        entry = ViewRequested(
            view_name=view_func.func_name,
            address=request.build_absolute_uri(),
            date=timezone.now(),
        )
        entry.save();

use_in_analytics = decorator_from_middleware(AnalysisMiddleware)
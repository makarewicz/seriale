from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('users.views',
    url(r'^(?P<username>\w+)/change_password', 'change_password'),
    url(r'^(?P<username>\w+)', 'profile_page'),
)
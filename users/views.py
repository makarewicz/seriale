# Create your views here.
from django.contrib.auth.models import User
from django.template.response import TemplateResponse
from django.shortcuts import redirect

def profile_page(request, username, messages=None):
    if request.user.is_authenticated() and (request.user.username == username or request.user.is_staff):
        print messages
        context = {'user': User.objects.get(username__exact=username),
                   'messages': messages,}
        return TemplateResponse(request, 'user/profile.html',
            context)
    else:
        return redirect('access-denied')

def change_password(request, username):
    if request.user.is_authenticated() and (request.user.username == username or request.user.is_staff):
        password1 = request.POST.get('password1')
        password2 = request.POST.get('password2')
        if password1 != password2:
            return profile_page(request, username, ["Passwords don't match"])
        u = User.objects.get(username__exact=username)
        u.set_password(password1)
        u.save()
        return profile_page(request, username)
    else:
        return redirect('access-denied')
from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('opis_z_wiki.views',
    url(r'^(?P<show_name>\w+)/obrazek(?P<img_nr>\d+).*', 'get_img_from_wiki'),
    url(r'^(?P<show_name>\w+)', 'get_description_from_wiki'),
)
# Create your views here.
import urllib2
from django.http import HttpResponse
from bs4 import BeautifulSoup
import re
import mimetypes

def get_html_from_show_name(show_name):
    url = 'http://en.wikipedia.org/wiki/' + show_name
    headers = { 'User-Agent' : 'Mozilla/5.0' }
    req = urllib2.Request(url, None, headers)
    html = urllib2.urlopen(req).read()
    return html

def remove_unnecessary_tags(content):
    classes_to_remove = ["infobox vevent", "dablink", "toc", "navbox", "reference", "editsection",
                         re.compile(r'reflist'), re.compile(r'metadata'), re.compile(r'rellink')]
    for class_to_remove in classes_to_remove:
        to_remove_list = content.find_all(attrs = {"class": class_to_remove})
        for to_remove in to_remove_list:
            to_remove.decompose()
    for link in content.find_all('a'):
        link.unwrap()

def get_clean_content_from_show_name(show_name):
    html = get_html_from_show_name(show_name)
    soup = BeautifulSoup(html)
    content = soup.find(id='mw-content-text')
    remove_unnecessary_tags(content)
    return content

def get_description_from_wiki_raw(show_name, change_img_sources=True):
    content = get_clean_content_from_show_name(show_name)
    if(change_img_sources):
        # Changing img sources.
        for (counter, img) in enumerate(content.find_all('img')):
            file_ext = img['src'].split('.')[-1]
            img['src'] = show_name +'/obrazek{0}.{1}'.format(counter + 1, file_ext)
    return content

def get_description_from_wiki(request, show_name):
    return HttpResponse(get_description_from_wiki_raw(show_name))

def get_image_url_from_wiki(show_name, img_nr):
    img_nr = int(img_nr)
    content = get_clean_content_from_show_name(show_name)
    img = content.find_all('img')[img_nr - 1]
    return 'http:' + img['src']

def get_img_from_wiki(request, show_name, img_nr):
    url = get_image_url_from_wiki(show_name, img_nr)
    headers = { 'User-Agent' : 'Mozilla/5.0' }
    req = urllib2.Request(url, None, headers)
    img = urllib2.urlopen(req).read()
    return HttpResponse(img, mimetype=mimetypes.guess_type(url))